# HoloLens Spectator View Demo
This repository contains a Unity3D project which shows the Spectator View technique provided by Microsoft MixedRealityToolkit.

If you are new to this topic read the instruction on the documentation page of this feature.
https://github.com/Microsoft/MixedRealityToolkit-Unity/tree/master/Assets/HoloToolkit-Preview/SpectatorView

This project was buit with Unity3D Version 2017.4.14f and HoloToolkit 2017.4.3.0